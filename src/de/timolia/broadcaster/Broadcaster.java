package de.timolia.broadcaster;

import com.google.common.collect.Maps;
import com.google.gson.Gson;
import de.timolia.broadcaster.database.Sql;
import de.timolia.broadcaster.database.tables.BroadcastMessage;
import de.timolia.broadcaster.database.tables.BroadcastSetting;
import de.timolia.broadcaster.functions.Jobber;
import de.timolia.broadcaster.functions.Messenger;
import de.timolia.broadcaster.util.QuickJoin;
import de.timolia.bungee.Dispatcher;
import de.timolia.bungee.TBungee;
import net.md_5.bungee.api.plugin.Plugin;

import java.util.Map;
import java.util.Set;

/**
 * @author Shustin
 */
public class Broadcaster extends Plugin {

    private Dispatcher dispatcher;
    private Messenger messenger;
    private Gson gson;

    private Map<String, Set<BroadcastMessage>> messageMap;
    private Map<String, BroadcastSetting> settingMap;

    private static Broadcaster instance;

    @Override
    public void onLoad() {
        Dispatcher.addBundle("broadcaster", getClass().getClassLoader());
        dispatcher = new Dispatcher("Timolia", getLogger());
        gson = new Gson();
        messageMap = Maps.newConcurrentMap();
        settingMap = Maps.newConcurrentMap();
    }

    @Override
    public void onEnable() {
        instance = this;
        TBungee.instance.register(BroadcastCommand.class);
        TBungee.instance.register(QuickJoin.class);
        messenger = new Messenger();
        new Jobber();
        reload();
    }

    public void reload() {
        Sql.reloadData();
    }

    public static Broadcaster getInstance() {
        return instance;
    }

    public Dispatcher getDispatcher() {
        return dispatcher;
    }

    public Messenger getMessenger() {
        return messenger;
    }

    public Gson getGson() {
        return gson;
    }

    public Set<BroadcastMessage> getMessages(String key) {
        return messageMap.get(key);
    }

    public BroadcastSetting getSetting(String key) {
        return settingMap.get(key);
    }

    public void setMessageMap(Map<String, Set<BroadcastMessage>> messageMap) {
        this.messageMap = messageMap;
    }

    public void setSettingMap(Map<String, BroadcastSetting> settingMap) {
        this.settingMap = settingMap;
    }

    public Map<String, Set<BroadcastMessage>> getMessageMap() {
        return messageMap;
    }

    public Map<String, BroadcastSetting> getSettingMap() {
        return settingMap;
    }
}
