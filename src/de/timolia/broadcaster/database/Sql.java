package de.timolia.broadcaster.database;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import de.timolia.broadcaster.BroadcastCommand;
import de.timolia.broadcaster.Broadcaster;
import de.timolia.broadcaster.database.tables.BroadcastMessage;
import de.timolia.broadcaster.database.tables.BroadcastSetting;
import de.timolia.bungee.TBungee;
import de.timolia.core.util.exec.ThreadExecutor;
import de.timolia.database.SqlServer;

import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Shustin
 */
public class Sql {

    private static SqlServer sqlServer = TBungee.sqlServer;

    /**
     * Load all BroadcastMessages and all BroadcastSettings from sql and put them into a ConcurrentHashMap.
     */

    public static void reloadData() {
        ThreadExecutor.executeAsync(() -> {
            Map<String, Set<BroadcastMessage>> messageMap = Maps.newConcurrentMap();
            sqlServer.find(BroadcastMessage.class).where().findSet().forEach(broadcastMessage -> {
                if(messageMap.get(broadcastMessage.getKey()) != null) {
                    messageMap.get(broadcastMessage.getKey()).add(broadcastMessage);
                } else {
                    messageMap.put(broadcastMessage.getKey(), Sets.newHashSet(broadcastMessage));
                }
            });
            Broadcaster.getInstance().setSettingMap(sqlServer.find(BroadcastSetting.class).findSet().stream().collect(Collectors.toConcurrentMap(BroadcastSetting::getKey, broadcastSetting -> broadcastSetting)));
        });
    }

    /**
     * Update, insert or delete a object
     *
     * @param action        type of action
     * @param object        object to work with
     */

    public static void sqlAction(Action action, Object object) {
        if(action == Action.INSERT) {
            sqlServer.insert(object);
        } else if (action == Action.UPDATE) {
            sqlServer.update(object);
        } else if (action == Action.DELETE) {
            sqlServer.delete(object);
        }
        BroadcastCommand.update();
    }

    public enum Action {
        INSERT,
        UPDATE,
        DELETE,
        NOW;
    }
}
