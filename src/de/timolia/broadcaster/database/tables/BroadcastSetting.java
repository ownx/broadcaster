package de.timolia.broadcaster.database.tables;

import de.timolia.broadcaster.Broadcaster;
import de.timolia.broadcaster.util.Serializer;
import de.timolia.broadcaster.util.Tags;
import de.timolia.broadcaster.util.TaskData;
import de.timolia.database.annotations.Id;
import de.timolia.database.annotations.Table;

/**
 * @author Shustin
 */
@Table(value = "timolia_broadcast_settings")
public class BroadcastSetting {

    @Id
    private String key;

    private String server;

    private String tags;

    private transient boolean active;

    private transient String taskData;

    public BroadcastSetting() {

    }

    /**
     * This represents a deserialized BroadcastSetting without transient parameters.
     *
     * @param key
     * @param server
     * @param tags
     */

    public BroadcastSetting(String key, String server, Tags... tags) {
        this.key = key;
        this.server = server;
        this.setTags(tags);
    }

    /**
     * This represents a default BroadcastSetting from sql.
     *
     * @param key
     * @param server
     * @param active
     * @param taskData
     * @param tags
     */

    public BroadcastSetting(String key, String server, boolean active, String taskData, Tags... tags) {
        this(key, server, tags);
        this.active = active;
        this.taskData = taskData;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public void setTags(Tags... tags) {
        this.tags = Broadcaster.getInstance().getGson().toJson(tags);
    }

    public String getKey() {
        return key;
    }

    public String getServer() {
        return server;
    }

    public Tags[] getTags() {
        return Broadcaster.getInstance().getGson().fromJson(this.tags, Tags[].class);
    }

    public TaskData getTaskData() {
        return (TaskData) Serializer.deserialize(this.taskData, TaskData.class);
    }
}
