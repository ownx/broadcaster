package de.timolia.broadcaster.database.tables;

import de.timolia.database.annotations.Id;
import de.timolia.database.annotations.Table;

/**
 * @author Shustin
 */
@Table(value = "timolia_broadcast_message")
public class BroadcastMessage {

    @Id
    private int id;

    private String language;

    private String key;

    private String message;

    public BroadcastMessage() {

    }

    public BroadcastMessage(int id, String language, String key, String message) {
        this.id = id;
        this.language = language;
        this.key = key;
        this.message = message;
    }

    public BroadcastMessage(String language, String key, String message) {
        this.language = language;
        this.key = key;
        this.message = message;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getId() {
        return id;
    }

    public String getLanguage() {
        return language;
    }

    public String getKey() {
        return key;
    }

    public String getMessage() {
        return message;
    }

}