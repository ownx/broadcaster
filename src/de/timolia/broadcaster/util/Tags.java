package de.timolia.broadcaster.util;

/**
 * @author Shustin
 */
public enum Tags {

    /**
     * Tags are for filtering players if they're allowed to receive messages.
     */

    FORUM,
    NO_FORUM,
    PRO,
    NO_PRO,
    EXPERT,
    NO_EXPERT,;

}
