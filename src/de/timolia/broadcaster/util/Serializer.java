package de.timolia.broadcaster.util;

import de.timolia.broadcaster.Broadcaster;

/**
 * @author Shustin
 */
public class Serializer {

    /**
     * Static method to serialize objects (i didn't want to type Broadcaster.getInstance..... every time :P)
     *
     * @param object
     * @param classs
     * @return
     */

    public static String serialize(Object object, Class<?> classs) {
        return Broadcaster.getInstance().getGson().toJson(object, classs);
    }

    /**
     * Static method to deserialize objects (i didn't want to type Broadcaster.getInstance..... every time :P)
     *
     * @param json
     * @param classs
     * @return
     */

    public static Object deserialize(String json, Class<?> classs) {
        return Broadcaster.getInstance().getGson().fromJson(json, classs);
    }

}
