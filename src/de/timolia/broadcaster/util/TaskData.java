package de.timolia.broadcaster.util;

import java.lang.reflect.Field;
import java.util.Calendar;

/**
 * @author Shustin
 */
public class TaskData {

    private String minute;
    private String hour;
    private String day;
    private String month;
    private String weekDay;

    /**
     * "cron expression"
     *
     * @param minute
     * @param hour
     * @param day
     * @param month
     * @param weekDay
     */

    public TaskData(String minute, String hour, String day, String month, String weekDay) {
        this.minute = minute;
        this.hour = hour;
        this.day = day;
        this.month = month;
        this.weekDay = weekDay;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public void setMinute(String minute) {
        this.minute = minute;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public void setWeekDay(String weekDay) {
        this.weekDay = weekDay;
    }

    public String getDay() {
        return day;
    }

    public String getHour() {
        return hour;
    }

    public String getMinute() {
        return minute;
    }

    public String getMonth() {
        return month;
    }

    public String getWeekDay() {
        return weekDay;
    }

    /**
     * Checks if the "cron expression" matches the current time
     *
     * @param taskData given "cron expression"
     * @return
     */

    public static boolean isNow(TaskData taskData) {
        Calendar calendar = Calendar.getInstance();
        int[] currentData = new int[]{calendar.get(Calendar.MINUTE), calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.DAY_OF_MONTH), calendar.get(Calendar.MONTH)+1, calendar.get(Calendar.DAY_OF_WEEK)-1};
        for(int i = 0; i < taskData.getClass().getDeclaredFields().length; i++) {
            Field field = taskData.getClass().getDeclaredFields()[i];
            try {
                String value = (String) field.get(taskData);
                if(value.equals("*")) continue;
                if(value.startsWith("\\")) {
                    boolean plus = value.endsWith("+");
                    int number = Integer.parseInt(value.substring(1, plus ? value.length()-1 : value.length()));
                    if(plus) {
                        if(currentData[i] == 0 || (currentData[i] % number+1 != 1 && currentData[i] != 1)) return false;
                    } else {
                        if(currentData[i] % number != 0 && currentData[i] != 0) return false;
                    }
                    continue;
                }
                int number = Integer.parseInt(value);
                if(number != currentData[i]) return false;
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return true;
    }

}
