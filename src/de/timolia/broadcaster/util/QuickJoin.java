package de.timolia.broadcaster.util;

import de.timolia.core.functions.TCommand;
import net.md_5.bungee.api.connection.ProxiedPlayer;

/**
 * @author Shustin
 */
public class QuickJoin {

    /**
     * Sendet Spieler per Click in einer Nachricht zur Cloud
     *
     * @param proxiedPlayer     player to send
     * @param arg               server cloud
     */

    @TCommand(name = "qj", parent = "broadcaster")
    public void quickJoin(ProxiedPlayer proxiedPlayer, String arg) {
        //TODO: sende zur server-cloud
    }

}
