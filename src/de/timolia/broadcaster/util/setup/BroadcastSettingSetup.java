package de.timolia.broadcaster.util.setup;

import de.timolia.broadcaster.database.tables.BroadcastSetting;

public class BroadcastSettingSetup extends BroadcastSetup {

        private BroadcastSetting broadcastSetting;

        public BroadcastSettingSetup(BroadcastSetting broadcastSetting) {
            this.broadcastSetting = broadcastSetting;
        }

        public BroadcastSetting getBroadcastSetting() {
            return broadcastSetting;
        }

        public void setBroadcastSetting(BroadcastSetting broadcastSetting) {
            this.broadcastSetting = broadcastSetting;
        }
    }
