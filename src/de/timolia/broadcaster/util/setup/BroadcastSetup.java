package de.timolia.broadcaster.util.setup;

import de.timolia.broadcaster.database.Sql;

/**
 * @author Shustin
 */
public abstract class BroadcastSetup {

    private boolean complete;

    private Sql.Action action;

    public Sql.Action getAction() {
        return action;
    }

    public boolean isComplete() {
        return complete;
    }

    public void setAction(Sql.Action action) {
        this.action = action;
    }

    public void setComplete(boolean complete) {
        this.complete = complete;
    }
}

