package de.timolia.broadcaster.util.setup;

import de.timolia.broadcaster.database.tables.BroadcastMessage;

public class BroadcastMessageSetup extends BroadcastSetup {

    private BroadcastMessage broadcastMessage;

    public BroadcastMessageSetup(BroadcastMessage broadcastMessage) {
        this.broadcastMessage = broadcastMessage;
    }

    public BroadcastMessage getBroadcastMessage() {
        return broadcastMessage;
    }

    public void setBroadcastMessage(BroadcastMessage broadcastMessage) {
        this.broadcastMessage = broadcastMessage;
    }
}
