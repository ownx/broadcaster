package de.timolia.broadcaster.util.setup;

public class BroadcastNowSetup extends BroadcastSetup {

        private BroadcastMessageSetup broadcastMessageSetup;
        private BroadcastSettingSetup broadcastSettingSetup;

        public BroadcastMessageSetup getBroadcastMessageSetup() {
            return broadcastMessageSetup;
        }

        public BroadcastSettingSetup getBroadcastSettingSetup() {
            return broadcastSettingSetup;
        }

        public void setBroadcastMessageSetup(BroadcastMessageSetup broadcastMessageSetup) {
            this.broadcastMessageSetup = broadcastMessageSetup;
        }

        public void setBroadcastSettingSetup(BroadcastSettingSetup broadcastSettingSetup) {
            this.broadcastSettingSetup = broadcastSettingSetup;
        }
    }
