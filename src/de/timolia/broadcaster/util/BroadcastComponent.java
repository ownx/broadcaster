package de.timolia.broadcaster.util;

/**
 * @author Shustin
 */
public class BroadcastComponent {

    private String text;
    private String action;
    private String actionValue;

    public BroadcastComponent(BroadcastComponent broadcastComponent) {
        this.text = broadcastComponent.getText();
        this.action = broadcastComponent.getAction();
        this.actionValue = broadcastComponent.getActionValue();
    }

    public String getActionValue() {
        return actionValue;
    }

    public String getAction() {
        return action;
    }

    public String getText() {
        return text;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setActionValue(String actionValue) {
        this.actionValue = actionValue;
    }

    public BroadcastComponent duplicate() {
        return new BroadcastComponent(this);
    }
}
