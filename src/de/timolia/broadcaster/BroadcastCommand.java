package de.timolia.broadcaster;

import com.google.common.collect.Maps;
import de.timolia.broadcaster.database.Sql;
import de.timolia.broadcaster.database.tables.BroadcastMessage;
import de.timolia.broadcaster.database.tables.BroadcastSetting;
import de.timolia.broadcaster.util.Serializer;
import de.timolia.broadcaster.util.setup.BroadcastMessageSetup;
import de.timolia.broadcaster.util.setup.BroadcastNowSetup;
import de.timolia.broadcaster.util.setup.BroadcastSettingSetup;
import de.timolia.broadcaster.util.setup.BroadcastSetup;
import de.timolia.core.functions.TCommand;
import de.timolia.core.redis.TRedis;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.Map;
import java.util.Optional;

/**
 * @author Shustin
 */
public class BroadcastCommand {

    private Map<CommandSender, BroadcastSetup> setupMap = Maps.newHashMap();

    public BroadcastCommand() {
        TRedis.subscribe("broadcaster.send", message -> {
            String[] data = message.split("SPLIT");
            if(data.length != 2) {
                //TODO: Handle
                return;
            }
            BroadcastMessage broadcastMessage = (BroadcastMessage) Serializer.deserialize(data[0], BroadcastMessage.class);
            BroadcastSetting broadcastSetting = (BroadcastSetting) Serializer.deserialize(data[1], BroadcastSetting.class);
            Broadcaster.getInstance().getMessenger().sendLocal(broadcastMessage, broadcastSetting);
        });
        TRedis.subscribe("broadcaster.update", message -> Broadcaster.getInstance().reload());
    }

    /**
     * sends a notification to all bungees that an update is available
     */

    public static void update() {
        TRedis.publish("broadcaster.update", "update");
    }

    /**
     * sends serialized data to all bungees that players on other bungees will get the broadcast
     *
     * @param broadcastMessage
     * @param broadcastSetting
     */

    public static void send(BroadcastMessage broadcastMessage, BroadcastSetting broadcastSetting) {
        TRedis.publish("broadcaster.send", Serializer.serialize(broadcastMessage, BroadcastMessage.class) + "SPLIT" + Serializer.serialize(broadcastSetting, BroadcastSetting.class));
    }

    @TCommand(
            name = "broadcaster",
            aliases = {"br", "broadcast"},
            usage = "/broadcaster",
            description = "Erstelle, bearbeite oder versende Broadcasts"
    )

    public void onCommand(CommandSender commandSender, String arg0, String args) {
        if(!(commandSender instanceof ProxiedPlayer)) {
            if(arg0.equalsIgnoreCase("create") || arg0.equalsIgnoreCase("list")) {
                return;
            }
        }
        if(arg0.equalsIgnoreCase("now")) {
            addSetup(commandSender, Sql.Action.NOW, new BroadcastNowSetup());
        } else if(arg0.equalsIgnoreCase("create")) {
            if(args.contains("setting")) {
                addSetup(commandSender, Sql.Action.INSERT, new BroadcastSettingSetup(new BroadcastSetting()));
            } else {
                addSetup(commandSender, Sql.Action.INSERT, new BroadcastMessageSetup(new BroadcastMessage()));
            }
        } else if(arg0.equalsIgnoreCase("update")) {
            if(args.contains("setting")) {
                addSetup(commandSender, Sql.Action.UPDATE, new BroadcastSettingSetup(new BroadcastSetting()));
            } else {
                addSetup(commandSender, Sql.Action.UPDATE, new BroadcastMessageSetup(new BroadcastMessage()));
            }
        } else if(arg0.equalsIgnoreCase("delete")) {
            String[] x = args.split(" ");
            if(args.contains("setting")) {
                String key = x[x.length-1];
                Sql.sqlAction(Sql.Action.DELETE, Broadcaster.getInstance().getSetting(key));
            } else {
                String key = x[x.length-2];
                String language = x[x.length-2];
                Optional<BroadcastMessage> broadcastMessage = Broadcaster.getInstance().getMessages(key).stream().filter(broadcastMessage1 -> broadcastMessage1.getLanguage().equalsIgnoreCase(language)).findAny();
                broadcastMessage.ifPresent(broadcastMessage1 -> Sql.sqlAction(Sql.Action.DELETE, broadcastMessage1));
            }
        } else if(arg0.equalsIgnoreCase("confirm")) {
            if(!setupMap.containsKey(commandSender)) {
                //TODO: sendMessage (nothing to confirm)
                return;
            }
            BroadcastSetup broadcastSetup = setupMap.get(commandSender);
            if(!broadcastSetup.isComplete()) {
                //TODO: sendMessage (setup isn't finished)
                return;
            }
            if(broadcastSetup.getAction() == Sql.Action.NOW) {
                send(((BroadcastNowSetup)broadcastSetup).getBroadcastMessageSetup().getBroadcastMessage(), (((BroadcastNowSetup)broadcastSetup).getBroadcastSettingSetup().getBroadcastSetting()));
                //TODO: sendMessage (confirm successful)
                return;
            }
            Sql.sqlAction(broadcastSetup.getAction(), broadcastSetup);
            //TODO: sendMessage (confirm successful)
        } else if(arg0.equalsIgnoreCase("list")) {

        } else if(arg0.equalsIgnoreCase("setup")) {
            BroadcastSetup broadcastSetup = setupMap.get(commandSender);
            if(broadcastSetup == null) {
                //TODO: sendMessage
                return;
            }

        }
    }

    private void addSetup(CommandSender commandSender, Sql.Action sqlAction, BroadcastSetup broadcastSetup) {
        if(setupMap.containsKey(commandSender)) {
            //TODO: sendMessage (there is a setup left)
            return;
        }
        broadcastSetup.setAction(sqlAction);
        setupMap.put(commandSender, broadcastSetup);
    }

}
