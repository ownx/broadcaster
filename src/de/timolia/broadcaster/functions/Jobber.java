package de.timolia.broadcaster.functions;

import de.timolia.broadcaster.Broadcaster;
import de.timolia.broadcaster.util.TaskData;
import net.md_5.bungee.api.ProxyServer;

import java.util.concurrent.TimeUnit;

/**
 * @author Shustin
 */
public class Jobber {

    public Jobber() {
        start();
    }

    /**
     * Just check every minute if a BroadcastSetting - TaskData matches current timestamp.
     */

    private void start() {
        ProxyServer.getInstance().getScheduler().schedule(Broadcaster.getInstance(), () -> Broadcaster.getInstance().getSettingMap().values().stream()
                .filter(broadcastSetting -> TaskData.isNow(broadcastSetting.getTaskData()))
                .forEach(broadcastSetting -> Broadcaster.getInstance().getMessages(broadcastSetting.getKey()).forEach(broadcastMessage -> Broadcaster.getInstance().getMessenger().sendLocal(broadcastMessage, broadcastSetting))),
                0L, 1L, TimeUnit.MINUTES);
    }
}
