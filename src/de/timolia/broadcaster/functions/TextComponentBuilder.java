package de.timolia.broadcaster.functions;

import com.google.common.collect.Lists;
import de.timolia.broadcaster.util.BroadcastComponent;
import de.timolia.broadcaster.util.Serializer;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Shustin
 */
public class TextComponentBuilder {

    private String text;
    private List<TextComponent> textComponents = Lists.newArrayList();

    /**
     * Creates a new TextComponentBuilder and replaces all matching Strings with an code.
     *
     * @param json      Plain text with serialized BroadcastComponents
     */

    public TextComponentBuilder(String json) {
        List<String> componentsStrings = Lists.newArrayList();

        String regex = "@\\[(.*?)]";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(json);

        while (matcher.find()) {
            String matchingString = matcher.group();
            json = json.replace(matchingString, "@" + componentsStrings.size());
            componentsStrings.add(matchingString);
        }
        componentsStrings.forEach(s -> textComponents.add(buildComponent((BroadcastComponent) Serializer.deserialize(s, BroadcastComponent.class))));
        text = json;
    }

    /**
     * Builds a TextComponent with the data of a BroadcastComponent.
     *
     * @param broadcastComponent
     * @return
     */

    private TextComponent buildComponent(BroadcastComponent broadcastComponent) {
        TextComponent textComponent = new TextComponent(broadcastComponent.getText());
        if(broadcastComponent.getAction() != null) {
            if(broadcastComponent.getAction().startsWith("hover")) {
                if(broadcastComponent.getAction().contains(":")) {
                    String[] actions = broadcastComponent.getActionValue().split(":");
                    BroadcastComponent clickComponent = broadcastComponent.duplicate();
                    clickComponent.setAction(broadcastComponent.getAction().split(":")[1]);
                    clickComponent.setActionValue(actions[1]);
                    BroadcastComponent hoverComponent = broadcastComponent.duplicate();
                    hoverComponent.setAction("hover");
                    hoverComponent.setActionValue(actions[0]);
                    textComponent = setClickEvent(textComponent, clickComponent);
                    textComponent = setHoverEvent(textComponent, hoverComponent);
                } else {
                    textComponent.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(broadcastComponent.getActionValue()).create()));
                }
            } else {
                setClickEvent(textComponent, broadcastComponent);
            }
        }
        return textComponent;
    }

    private TextComponent setClickEvent(TextComponent textComponent, BroadcastComponent broadcastComponent) {
        ClickEvent.Action action = ClickEvent.Action.valueOf(broadcastComponent.getAction().toUpperCase());
        textComponent.setClickEvent(new ClickEvent(action, broadcastComponent.getActionValue()));
        return textComponent;
    }

    private TextComponent setHoverEvent(TextComponent textComponent, BroadcastComponent broadcastComponent) {
        textComponent.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(broadcastComponent.getActionValue()).create()));
        return textComponent;
    }

    /**
     * Builds a TextComponent out of the TextComponents and the Text from the TextComponentBuilder
     *
     * @param chatColor     Color of TextComponent
     * @return              TextComponent out of all TextComponents that were build before
     */

    public TextComponent create(ChatColor chatColor) {
        TextComponent textComponent = new TextComponent();
        textComponent.setColor(chatColor);
        int index = -1;
        String finalText = text;
        while (finalText.matches("@[0-9]")) {
            String[] splitText = finalText.split("@" + index++);
            TextComponent extra = new TextComponent(splitText[0]);
            textComponent.addExtra(textComponents.get(index));
            finalText = splitText[1];
            textComponent.addExtra(extra);
        }
        return textComponent;
    }

}
