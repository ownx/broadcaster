package de.timolia.broadcaster.functions;

import de.timolia.broadcaster.database.tables.BroadcastMessage;
import de.timolia.broadcaster.database.tables.BroadcastSetting;
import de.timolia.broadcaster.util.Tags;
import de.timolia.core.database.entities.User;
import de.timolia.core.functions.LocalizationCache;
import de.timolia.core.redis.TRedis;
import de.timolia.core.util.UserIndex;
import de.timolia.core.util.exec.ThreadExecutor;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

/**
 * @author Shustin
 */
public class Messenger {

    /**
     * Verschickt eine serialisierte Nachricht an alle Bungees,
     * welche als erstes die serialisierte BroadcastMessage enthält,
     * worauf ein SPLIT folgt um die darauffolgende serialisierte BroadcastSetting
     * und die BroadcastMessage von einander zu trennen.
     *
     * @param content (serialisierter Json)
     *
     */

    public void sendBroadcast(String content) {
        TRedis.publish("broadcaster.send", content);
    }

    /**
     * Sendet eine Broadcast auf diesem Bungee
     *
     * @param broadcastMessage      Nachricht
     * @param broadcastSetting      Einstellungen
     *
     * @see Messenger#sendLocal(String, String, String, Tags...)
     */

    public void sendLocal(BroadcastMessage broadcastMessage, BroadcastSetting broadcastSetting) {
        sendLocal(broadcastMessage.getMessage(), broadcastMessage.getLanguage(), broadcastSetting.getServer(), broadcastSetting.getTags());
    }

    /**
     * Sendet eine Broadcast auf diesem Bungee
     *
     * @param message       zu versendene Nachricht
     * @param language      Sprache, die der Spieler haben muss
     * @param server        Server, auf dem sich der Spieler befinden muss
     * @param tags          Bedingungen, die der Spieler erfüllen muss
     *
     * @see Messenger#sendLocal(BroadcastMessage, BroadcastSetting)
     */

    private void sendLocal(String message, String language, String server, Tags... tags) {
        TextComponent textComponent = new TextComponentBuilder(message).create(ChatColor.GRAY);
        ThreadExecutor.executeAsync(() -> ProxyServer.getInstance().getPlayers().stream()
                .filter(proxiedPlayer -> hasLanguage(proxiedPlayer, language))
                .filter(proxiedPlayer -> proxiedPlayer.getServer().getInfo().getName().contains(server))
                .filter(proxiedPlayer -> isTagged(proxiedPlayer, tags))
                .forEach(proxiedPlayer -> {
                    TextComponent finalText = (TextComponent) textComponent.duplicate();
                    finalText.setText(replacedText(finalText.getText(), proxiedPlayer));
                    proxiedPlayer.sendMessage(textComponent);
                }));
    }

    private String replacedText(String toReplace, ProxiedPlayer proxiedPlayer) {
        if(toReplace.contains("%p%")) toReplace = toReplace.replace("%p%", proxiedPlayer.getName());
        //TODO: falls noch mehr einfällt, kannst was hinzufügen :D
        return toReplace;
    }

    /**
     * Checked, ob der Spieler die benötigten Bedingungen für das Empfangen der Nachricht erfüllt.
     * TODO: Die Namen der Tags sind selbsterklärend, in der Schleife muss noch gechecked werden,
     * TODO: ob der Spieler im Forum angemeldet ist, ob er Pro hat, ob er Expert hat und alles nochmal anders herum, ob es nicht so ist!
     *
     * @param proxiedPlayer     geprüfter Spieler
     * @param tags              Bedingungen
     *
     * @return if player is tagged by given tags
     *
     * @see Tags
     */

    private boolean isTagged(ProxiedPlayer proxiedPlayer, Tags... tags) {
        User user = UserIndex.getUser(proxiedPlayer.getUniqueId());
        for(Tags toCheck : tags) {
            //TODO: hier die Tags checken lassen
        }
        return true;
    }

    /**
     *
     * @param player        geprüfter Spieler
     * @param language      zu prüfende Sprache
     * @return if player has given language
     */

    private boolean hasLanguage(ProxiedPlayer player, String language) {
        return LocalizationCache.getLocalizationCache().getLanguageForPlayer(player.getUniqueId()).getCode().equalsIgnoreCase(language);
    }

}
